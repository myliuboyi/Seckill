package org.learn.service;

import org.learn.entity.SuccessKilled;

import java.util.List;

/**
 * Created by 刘博义 on 2017/10/30.
 */
public interface SuccessKilledService {

    List<SuccessKilled> getSuccessKilledList(long seckillId);

}
