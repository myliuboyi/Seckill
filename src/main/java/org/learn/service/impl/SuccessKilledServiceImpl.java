package org.learn.service.impl;

import org.learn.dao.SuccessKilledDao;
import org.learn.entity.SuccessKilled;
import org.learn.service.SuccessKilledService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 刘博义 on 2017/10/30.
 */
@Service
public class SuccessKilledServiceImpl implements SuccessKilledService {

    @Autowired
    private SuccessKilledDao successKilledDao;

    public List<SuccessKilled> getSuccessKilledList(long seckillId) {

        List<SuccessKilled> successKilledList = successKilledDao.queryByPhone(seckillId);
        for (SuccessKilled successKilledItem : successKilledList) {
            String userPhone = String.valueOf(successKilledItem.getUserPhone());
            StringBuffer phone = new StringBuffer(userPhone);
            userPhone = phone.delete(3,8).toString();
            successKilledItem.setUserPhone(Long.parseLong(userPhone));
        }
        return successKilledList;

    }


}
