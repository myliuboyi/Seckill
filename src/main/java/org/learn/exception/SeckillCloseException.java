package org.learn.exception;

/**秒杀关闭异常
 * Created by 刘博义 on 2017/9/11.
 */
public class SeckillCloseException extends SeckillException{

    public SeckillCloseException(String message) {
        super(message);
    }

    public SeckillCloseException(String message, Throwable cause) {
        super(message, cause);
    }
}
