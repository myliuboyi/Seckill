package org.learn.exception;

/**
 * 秒杀相关业务异常
 * Created by 刘博义 on 2017/9/11.
 */
public class SeckillException extends RuntimeException{

    public SeckillException(String message) {
        super(message);
    }

    public SeckillException(String message, Throwable cause) {
        super(message, cause);
    }
}
