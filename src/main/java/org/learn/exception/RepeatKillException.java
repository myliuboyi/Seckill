package org.learn.exception;

/**
 * 重复秒杀异常（运行异常)
 * Created by 刘博义 on 2017/9/11.
 */
public class RepeatKillException  extends SeckillException{

    public RepeatKillException(String message) {
        super(message);
    }

    public RepeatKillException(String message, Throwable cause) {
        super(message, cause);
    }
}
