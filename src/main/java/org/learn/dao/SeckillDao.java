package org.learn.dao;

import org.apache.ibatis.annotations.Param;
import org.learn.entity.Seckill;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 刘博义 on 2017/9/9.
 */
public interface SeckillDao {


    /**
     * 减库存
     * @param seckillId
     * @param killTime
     * @return
     */
    int reduceNumber(@Param("seckillId") long seckillId,@Param("killTime") Date killTime);

    /**
     *根据ID查询秒杀对象
     * @param seckillId
     * @return
     */
    Seckill queryById(long seckillId);

    /**
     * 根据偏移量查询秒杀商品列表
     * @param offset
     * @param limit
     * @return
     */
    List<Seckill> queryAll(@Param("offset") int offset, @Param("limit") int limit);

    void killByProcedure(Map<String,Object> paramMap);

}
