package org.learn.dao;

import org.apache.ibatis.annotations.Param;
import org.learn.entity.SuccessKilled;

import java.util.List;

/**
 * Created by 刘博义 on 2017/9/9.
 */
public interface SuccessKilledDao {

    /**
     * 插入购买明细，可过滤重复秒杀
     * @param seckillId
     * @param userPhone
     * @return
     */
    int insertSuccessKilled(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);

    /**
     * 根据id查询Successkilled并携带秒杀产品对象实体
     * @param seckillId
     * @return
     */
    SuccessKilled queryByIdWithSeckill(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);

    List<SuccessKilled> queryByPhone(@Param("seckillId") long seckillId);

}
